---
title: "windows下配置ubuntu环境"
date: 2019-09-19 16:31:30
categories: ["cs"]
tags: ["cs"]
draft: false
---

# 安装Ubuntu 18.04
在 microsoft store中 安装 ubuntu 18.04

# 安装terminal
- option1 : 安装terminus
```
[https://github.com/eugeny/terminus](https://github.com/eugeny/terminus)
```

- option2 : 安装windows terminal
```
[https://github.com/microsoft/terminal](https://github.com/microsoft/terminal)
```

# 安装 Oh My Zsh
```
sudo apt-get install zsh
wget --no-check-certificate https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | sh
chsh -s /bin/zsh`  将默认的shell从bash改为zsh
sudo reboot`  重新启动
```
# 安装主题 Powerlevel10k
-   主题的路径在`~/.oh-my-zsh/themes`, 使某一主题生效的文件为`~/.zshrc`文件，找到`ZSH_THEME="robbyrussell"`一行（大概11行左右），把其注释掉，在下面添一行`ZSH_THEME="Powerlevel10k"`，之后关闭终端，再重启就好了。
-   注意：在`=`符号右边一定不要有空格，否则会报错找不到此主题，我就是在这里困了半个多小时。
```
git clone https://github.com/romkatv/powerlevel10k.git$ZSH_CUSTOM/themes/powerlevel10k
```
Then edit your  `~/.zshrc`  and set  `ZSH_THEME="powerlevel10k/powerlevel10k"`.

Also add the following:  `POWERLEVEL9K_MODE="awesome-patched"`

# 安装字体
推荐字体`MesloLGS NF`
## firaCode字体
```
[https://github.com/tonsky/FiraCode](https://github.com/tonsky/FiraCode)
```
```
https://github-production-release-asset-2e65be.s3.amazonaws.com/27574418/e6cf8f30-35ce-11e8-8e1d-5f1e904d37c5?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20190919%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20190919T095658Z&X-Amz-Expires=300&X-Amz-Signature=4f2d66a2dbf86daa73ab10b81f60f70a66128b4809c422b7c621b23802c02d17&X-Amz-SignedHeaders=host&actor_id=13159809&response-content-disposition=attachment%3B%20filename%3DFiraCode.zip&response-content-type=application%2Foctet-stream
```
##  Nerd-Fonts
```
[https://www.nerdfonts.com/](https://www.nerdfonts.com/)
```
# 效果图
![效果图](https://i.postimg.cc/pV11ngxQ/image.png)
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTExMTg0MDU1MTAsLTg2Njk0Njc5MCwxNj
QyNjMzNzY4LC03ODMwNDg0ODIsLTk5OTgxMjc4XX0=
-->