---
layout: post                         
title: "Pandas数据离散化"                 
date: 2019-05-10 19:51:02 +0700 
categories: ['python']
tags: ["python"] 
draft: false
---

# 数据离散化
## 1 .等距分割
>将连续型变量的`取值范围`均匀划成`n`等份，每份的间距相等。如年龄在0-100范围之间，分成`0-9`,`10-19`,...,`90-99`共10份，每份间距都是`10`.**向cut传入面元的数量，则会根据数据的最小值和最大值计算等长面元**。

```python
df = pd.DataFrame({"age": range(0, 101, 4)})  
df["ageCut10"] = pd.cut(df["age"], 10, precision=1)
```
![1.png-54kB][1]

## 2. 等频分割
> 把`样本`均匀分为`n`等份，每份内包含的观察点数相同。如把所有样本按年龄从小到大排序，分成你`n`份，每份样本人数相等。qcut可以根据样本分位数对数据进行面元划分。
```python
df = pd.DataFrame({"age": range(0, 101, 4)})  
df["ageCut10"] = pd.qcut(df["age"], 10, precision=1)
```
![2.png-54kB][2]

## 3.自定义距离分割
> 如年龄按照`bins=[0,18,30,60,100]`的间距分割。`labels=['未成年','青年',‘中年’,'老年']`
```python
df = pd.DataFrame({"age": range(1, 101, 4)})  
df["ageCut10"] = pd.cut(df["age"],  
  bins=[0,18,30,60,100],  
  labels=['未成年','青年','中年','老年'],  
  precision=1)
```
![3.png-54kB][3]

## 4.自定义分位数分割
> 如年龄按照`bins=[0,0.1,0.5,0.8,100]`的间距分割。
```python
df = pd.DataFrame({"age": range(1, 101, 4)})  
df["ageCut10"] = pd.qcut(df["age"],[0.0,0.5,0.8,1.0],precision=3)
```
![4.png-54kB][4]


  [1]:  http://static.zybuluo.com/zenzenzen/t0v7r8nqf0if6p1wh98p0f12/1.png
  [2]:  http://static.zybuluo.com/zenzenzen/zokzv38eiqgk8p6xdagdqkc0/2.png
  [3]:  https://lh3.googleusercontent.com/_qiW1zU2RqKV1obY5AYbBNNNeANMV312diyqS9YUvBkQmrwRIHAgqui5bI6MQ707eTItB0RG628
  [4]: https://lh3.googleusercontent.com/_qiW1zU2RqKV1obY5AYbBNNNeANMV312diyqS9YUvBkQmrwRIHAgqui5bI6MQ707eTItB0RG628
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTMzMTc4ODA3MSwtMTc0MDkxNzk3OSw3Nz
Q2NjM4MTEsODQ5NTk2MzIsMjEyNDU2ODY1NCwtMTE1ODkzOTg3
NCwxOTE5MjAyNjI4LDExOTg5NzE2MSwtNzUyODk3NDY0LC0xMj
k1NTI2ODI4LC0yMzc0NjcwNzVdfQ==
-->