---
title: "LSTM三道门"
date: 2019-09-18 16:31:30
tags: ["cv","pytorch"]
categories: ["deeplearning"]
draft: false
---
# LSTM原理
## 结构图
![结构图](https://img-blog.csdnimg.cn/20181205225311472.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzE2OTQ5NzA3,size_16,color_FFFFFF,t_70)

## 公式（三道门）
### LSTM设计初衷：
下图是RNN的抽象结构单元
![RNN](https://i.postimg.cc/y8Np3hy4/image.png)

与传统RNN相比 ，LSTM 增加了三道门，分别用来控制信息的输出量。
- 对于历史状态: $\[{H_{t - 1}}\]$ 加一道门，用来控制历史状态信息量，决定丢弃多少信息，${\sigma =1}$ 表示全部保留，${\sigma}=0$表示全部遗忘，我们称为遗忘门。
- 对于输入信息$X_{t-1}$,我们也希望有一道门来控制其输入量，我们称为`输入门` ，输入门决定历史状态的更新。
- 对于$t$时刻的输出信息，我们也希望有一道门来控制其输出大小，我们称为`输出门`。

于是就有了，下图是LSTM的抽象结构单元,有$\sigma$的地方就是一个门,
$\sigma$取值0~1.
![lstm](https://i.postimg.cc/5thsrhdb/image.png)

### 遗忘门
遗忘门，决定哪些东西被遗忘
$f$ 就是forget的意思。
![遗忘门](https://img-blog.csdnimg.cn/20181205230526552.png)

###  输入门
输入门 ，决定状态$C_t$的更新
$i$就是input的意思。
![输入门](https://img-blog.csdnimg.cn/20181205225815769.png)
$C_t$​状态更新，与遗忘门和输入门有关
$\tilde{C}_{t}$ 不会完全参与到$C_t$的更新中，因为有**输入门**控制。
${C}_{t-1}$ 也不会完全参与到$C_t$的更新中，因为有**遗忘门**控制。
![状态更新](https://img-blog.csdnimg.cn/20181206103646174.png)

### 输出门
输出门，决定$t$时刻的输出：
其中，$h_{t-1}$ 为为上个时间节点$t-1$时刻的输出，输出的维度可以设定.
例如pytorch中:
```python
rnn = nn.LSTM(10, 20, 2) #(input_size,hidden_size,num_layers)
```
hidden_size的维度，其实就是设定的hhh的维度，并且每一个时间节点t，都会有一个输出.
![输出门](https://img-blog.csdnimg.cn/20181205225905523.png)
下图给出了对于每个门的输入和输出。
![lstm flow](https://i.postimg.cc/J4TcvG9R/image.png)
下图说明了每个门的作用。
![enter image description here](https://i.postimg.cc/9FNZ1s7M/image.png)

- $h_{t}$ and $C_t$关系

 $${h_t} = {o_t} \cdot \tanh \left( {{C_t}} \right)$$
  
$h_t$经过了三道门，$C_t$是经过了两道门。
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTExODgzODkwODIsMTY0OTEwMjYyM119
-->